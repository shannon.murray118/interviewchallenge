﻿using System.Collections.Generic;
using System.Linq;

namespace LibertyCodingChallenge
{
    public class Orchard : IOrchard 
    {
        public int CalculateMaximumApples(int[] A, int K, int L)
        {
            var maximumApples = 0;
	        var totalNumberOfRowsToBeCollected = K + L;

            //No possible selection for non overlapping
            if (totalNumberOfRowsToBeCollected > A.Length){
                return -1;
            }

            //If the amount they each want to collect equals the length, they will collect all without overlapping.
            if (totalNumberOfRowsToBeCollected == A.Length)
            {
	            maximumApples += A.Sum();
	            return maximumApples;
            }

            var lowAmountToBeCollected = K < L ? K : L;
            var highAmountToBeCollected = K > L ? K : L;

            var lowAmountToBeCollectedSegments = new List<PossibleSegment>();
            var highAmountToBeCollectedSegments = new List<PossibleSegment>();

			var index = 0;

            while (index <= A.Length - lowAmountToBeCollected)
			{
				if (index <= (A.Length - highAmountToBeCollected))
				{
					highAmountToBeCollectedSegments.Add(SegmentInformation(highAmountToBeCollected, A, index));
				}
				lowAmountToBeCollectedSegments.Add(SegmentInformation(lowAmountToBeCollected, A, index));
				index++;
			}

            foreach(var lowerSegment in lowAmountToBeCollectedSegments)
			{
                foreach(var upperSegment in highAmountToBeCollectedSegments)
				{
                    if (!upperSegment.DoesOverlap(lowerSegment.Indexes) && !lowerSegment.DoesOverlap(upperSegment.Indexes))
					{
                        var total = upperSegment.SegmentSum + lowerSegment.SegmentSum;
						if (total > maximumApples)
						{
							maximumApples = total;
						}
					}
				}
			}

            return maximumApples;
        }

        public PossibleSegment SegmentInformation(int endOfSegmentIndex, int[] A, int startOfSegmentIndex)
		{
			var segment = new PossibleSegment();
			for (var i = 0; i < endOfSegmentIndex; i++)
			{
                segment.SegmentSum += A[startOfSegmentIndex + i];
                segment.Indexes.Add(startOfSegmentIndex + i);
			}
			return segment;
		}
    }
}
