﻿using System;
using System.Collections.Generic;

namespace LibertyCodingChallenge
{
    public class PossibleSegment
    {
        public int SegmentSum { get; set; }

        public List<int> Indexes = new List<int>();

        public bool DoesOverlap(List<int> keys)
        {
            foreach (var index in Indexes)
            {
                if (keys.Contains(index))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
