﻿using System;

namespace LibertyCodingChallenge
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var orchard = new Orchard();
            int[] testCaseA = { 6, 1, 4, 6, 3, 2, 7, 4 };
            int[] testCaseB = { 0, 6, 5, 2, 2, 5, 1, 9, 4 };
            int[] testCaseC = { 3, 8, 1, 3, 2, 1, 8, 9, 0 };
            int[] testCaseD = { 2, 1, 5, 6, 0, 9, 5, 0, 3, 8 };
	        
			/*
             * Testing example with total no. of rows chosen is less than full array,
             * and when the rows chosen equals the full array, and
             * when there are no possible segments as the rows chosen is greater than the array
             */
			TestOutput(orchard.CalculateMaximumApples(testCaseA, 3, 2), 24);
			TestOutput(orchard.CalculateMaximumApples(testCaseA, 1, 1), 13);
			TestOutput(orchard.CalculateMaximumApples(testCaseA, 4, 4), 33);
			TestOutput(orchard.CalculateMaximumApples(testCaseA, 4, 5), -1);

			TestOutput(orchard.CalculateMaximumApples(testCaseB, 1, 2), 20);
			TestOutput(orchard.CalculateMaximumApples(testCaseB, 4, 5), 34);
			TestOutput(orchard.CalculateMaximumApples(testCaseB, 5, 5), -1);

			TestOutput(orchard.CalculateMaximumApples(testCaseC, 3, 2), 29);
			TestOutput(orchard.CalculateMaximumApples(testCaseC, 4, 5), 35);
			TestOutput(orchard.CalculateMaximumApples(testCaseC, 5, 5), -1);

			TestOutput(orchard.CalculateMaximumApples(testCaseD, 4, 3), 31);
			TestOutput(orchard.CalculateMaximumApples(testCaseD, 5, 5), 39);
			TestOutput(orchard.CalculateMaximumApples(testCaseD, 6, 5), -1);
        }

        public static void TestOutput(int actualResult, int expectedResult)
        {
			var outputString = "Expected result " + expectedResult +
							  " does not equal the actual result " + actualResult + ".";
            if (actualResult == expectedResult)
            {
                outputString = "Expected result " + expectedResult
                                 + " does equal the actual result " + actualResult + ".";
            }
            Console.WriteLine(outputString);
        }
    }
}
