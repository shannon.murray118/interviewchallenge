﻿using System;
namespace LibertyCodingChallenge
{
    public interface IOrchard
    {
        int CalculateMaximumApples(int[] A, int K, int L);
        PossibleSegment SegmentInformation(int end, int[] A, int index);
    }
}
